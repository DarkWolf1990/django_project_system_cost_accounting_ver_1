from django.apps import AppConfig


class SystemCostAccoutingConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "system_cost_accouting_"
